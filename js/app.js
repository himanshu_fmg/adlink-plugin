var routerApp = angular.module('adlinkApp', ['ui.router', 'mainController','mainService']);

routerApp.config(function($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
    //$urlMatcherFactoryProvider.strictMode(false);
    $urlRouterProvider.otherwise('/');

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/:cat/:filter/:subcat/?page',
            onEnter:function($state, $stateParams)
            {
                if($stateParams.filter != null && $stateParams.subcat == null)
                {
                    $state.go('home');
                }
            },
            params: {
                cat: {
                    squash: true,
                    value: null
                },
                filter: {
                    squash: true,
                    value: null
                },
                subcat: {
                    squash: true,
                    value: null,
                },
                page: {
                    squash: true,
                    value: '1'
                }
            },
            views: {
                '' : {
                    templateUrl: 'home/partial-home.html',
                    controller: "homeController",
                },
                'subheader@home':{
                    templateUrl: 'home/partial-home-subheader.html',   
                },
                'sidebar@home': {
                    templateUrl: 'home/partial-home-sidebar.html',                   
                },
                'content@home': {
                    templateUrl: 'home/partial-home-content.html',    
                }
            }
        })
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('ad-detail', {
            url: '/adDetail/:adSlug/:category/:adId',
            views: {
                '': { 
                    templateUrl: 'ad-detail/partial-ad-detail.html',
                    controller: "adDetailController",
                },
                'subheader@ad-detail':{
                    templateUrl: 'ad-detail/partial-ad-detail-subheader.html',   
                },
                'content@ad-detail': {
                    templateUrl: 'ad-detail/partial-ad-detail-content.html', 
                },
                'sidebar@ad-detail': {
                    templateUrl: 'ad-detail/partial-ad-detail-sidebar.html', 
                }
            }
            
        });        
        
})
.directive('header', function() {
  return {
    templateUrl: 'header.html'
  };
})
.directive('footer', function() {
  return {
    templateUrl: 'footer.html'
  };
})
.run(['$rootScope', '$http', '$state', function($rootScope, $http, $state) {
    $rootScope.home = 'Home';
    $http.get('../config.json').then(function(response) {
        $rootScope.config = response.data;
    },
	function (error) {
		console.log(error);
	    //defered.reject(error);
	},
	function (notify) {
	    //defered.notify(notify);
	});
}]);