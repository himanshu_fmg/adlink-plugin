mainController.controller('homeController', function ($scope, $rootScope, $state, $http, $parse, $stateParams, $injector, AdService) {
    //Default Page Limit
    $scope.pageLimit = 20;
    //Get All Params
    var urlparams = $state.params;
    $scope.current_page = urlparams.page;
    //Filter Dropdown
    $scope.filters = {
        '-1': 'sort by',
        'newest': 'relevance',
        'oldest': 'oldest first',
        'lowest': 'lowest price',
        'highest': 'highest price',
        'latest': 'latest',
        'distance': 'near me',
    };
    $scope.adListFilter = 'relevance';

    var params = {
        config: $scope.config,
        CurrentPage: urlparams.page,
        PerPage: $scope.pageLimit,
        filter: urlparams.filter,
        category: urlparams.cat,
        subcategory: urlparams.subcat,
    };
    //Get Ads listing
	AdService.getAds(params).promise.then(function (response) {
        //start here
        var adrecord = response.data.data.AdRecord;

        //Check if ads are featured
        angular.forEach(adrecord, function (ad, key) {
            var slugtitle = ad.Title.toLowerCase();
            ad.slugcategory = ad.Category.replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '-');
            ad.slug  = slugtitle.replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '-');
            angular.forEach(ad.Upsells, function (upsell, key1) {
                if (upsell.UpsellKeyword == 'top_ad') {
                    ad.is_featured = true;
                }
            });
            adrecord[key] = ad;
        });

        //console.log(adrecord);
        /* Needs to comment start here */
        if(adrecord.length > $scope.pageLimit) {
            var ads = [];
            var start = $scope.pageLimit * ($scope.current_page - 1) + 1;

            var end = $scope.pageLimit * $scope.current_page;
            if(end > adrecord.length)
            {
                end = adrecord.length+1;
            }
            for (var i = start; i < end; i++) {
                var ad = adrecord[i];
                ads.push(ad);
            }
            $scope.ads = ads;
        }
        else {
            var ads = [];
            var start = $scope.pageLimit * ($scope.current_page - 1) + 1;
            var end = adrecord.length;

            for (var i = start; i < end; i++) {
                var ad = adrecord[i];
                ads.push(ad);
            }
            $scope.ads = ads;
        }
        /* ends here */


        //Enable this
        //$scope.ads = adrecord;
        var adcount = adrecord.length;
        $scope.no_of_pages = [];
        if(adcount > 0)
        {
            var pagecount = Math.ceil(adcount / 20);
            for(var i=1;i<=pagecount;i++)
            {
                $scope.no_of_pages.push(i);
            }
        }
        $scope.records = response.data.data.Navigation.Total;
    },
    function (error){

    });


	//Get Ads Filter
    AdService.getAdsFilter(params).promise.then(function (response) {
            var adfilters = response.data.data.Filters;
            $scope.adsFilter = adfilters;
        },
        function (error){

        });
}).directive('pagination', function($compile, $templateRequest){
    return {
        restrict: 'E',
        scope: {
            //two way binding
            numberofpages: '=',
            selectedpage: '=',
        },
        link:function(scope, element)
        {
            var numberofpages = scope.numberofpages;
            scope.$watch('numberofpages', function(value){
                if(typeof value != 'undefined' )
                {
                    scope.showprev = false;
                    scope.shownext = false;
                    scope.no_of_pages = value;
                    if(value.length >= 15) {
                        var initialval = parseInt(scope.selectedpage) - Math.floor(15 / 2);
                        var lastval = parseInt(scope.selectedpage) + Math.floor(15 / 2);
                        if (initialval <= 0) {
                            lastval = lastval + 1 -(initialval);
                            initialval = 1;
                        } else if (lastval >= value.length) {
                            initialval = initialval - (lastval - value.length);
                            lastval = value.length;
                        }
                        var no_of_pages = [];
                        for(var i = initialval; i<=lastval; i++)
                        {
                            no_of_pages.push(i);
                        }
                        scope.no_of_pages = no_of_pages;
                        /*$templateRequest('../../templates/pagination-1.html').then(function(html){
                            element.html(html);
                            $compile(element.contents())(scope);
                        });*/
                        if(scope.selectedpage > (Math.floor(15/2) + 1))
                        {
                            scope.showprev = true;
                        }
                        if(scope.selectedpage < (value.length - Math.floor(15/2)))
                        {
                            scope.shownext = true;
                        }
                    }
                }
            });
        },
        templateUrl: '../../templates/pagination.html'
    };
});