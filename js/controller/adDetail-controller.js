mainController.controller('adDetailController', function ($scope, $rootScope, $state, $http, $parse, $stateParams, $injector, AdService) {
    //Get All Params
    var urlparams = $state.params;
    $rootScope.backbutton = true;
    var params = {
        config: $scope.config,
        adId: urlparams.adId,
    };
    AdService.getAdDetail(params).promise.then(function (response) {
        var addata = response.data.data;
        params.userId = addata.UserID;
        AdService.getUserAds(params).promise.then(function (response) {
                var userAdRecord = response.data.data.AdRecord;
                $scope.userads_length = userAdRecord.length;
                angular.forEach(userAdRecord, function (ad, key) {
                    console.log(ad);
                    if(params.adId != ad.ID)
                    {
                        var slugtitle = ad.Title.toLowerCase();
                        ad.slugcategory = ad.Category.replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '-');
                        ad.slug  = slugtitle.replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '-');
                        angular.forEach(ad.Upsells, function (upsell, key1) {
                            if (upsell.UpsellKeyword == 'top_ad') {
                                ad.is_featured = true;
                            }
                        });
                        userAdRecord[key] = ad;
                    }
                });
            addata.UserAds = userAdRecord;
            $scope.ad = addata;
        },
        function(error){

        });
    },
    function (error){

    });

});