mainService.factory('AdService', ['$http', '$q', function($http, $q) {
    return {
        getAds: function (params)
        {
            var data = {};
            /*var promise_url = params.config.APP_BASE_URL + 'getAds/AppKey/' + config.APP_KEY;
                if (typeof params.currentPage != 'undefined')
                {
                    promise_url = promise_url+'/Page/'+params.currentPage;
                }
                if (typeof params.perPage != 'undefined')
                {
                    promise_url = promise_url+'/perPage/'+params.perPage;
                }
                if (params.category != null)
                {
                    promise_url = promise_url+'/Type/'+params.category;
                }
                if (params.filter != null)
                {
                    promise_url = promise_url+'/af-'+params.filter;
                }
                if (params.sybcategory != null)
                {
                    promise_url = promise_url+'/'+params.subcategory;
                }

            */
            var promise_url = "../../ads.json";
            var defered = $q.defer();
            var canceller = $q.defer();
            var cancel = function (reason) {
                canceller.resolve(reason);
            };

            $http.get(promise_url,
            {
                //params: data,
                timeout: canceller.promise,
                headers: {
                }
            }
            ).then(
                    function (data) {
                        defered.resolve(data);
                    },
                    function (error) {
                        defered.reject(error);
                    },
                    function (notify) {
                        defered.notify(notify);
                    }
            );
            return {
                promise: defered.promise,
                cancel: cancel
            };
        },
        getAdsFilter: function (params)
        {
            var data = {};
            /*var promise_url = params.config.APP_BASE_URL + 'getAdsFilters/AppKey/' + config.APP_KEY;

            */
            var promise_url = "../../ads-filter.json";
            var defered = $q.defer();
            var canceller = $q.defer();
            var cancel = function (reason) {
                canceller.resolve(reason);
            };

            $http.get(promise_url,
                {
                    //params: data,
                    timeout: canceller.promise,
                    headers: {
                    }
                }
            ).then(
                function (data) {
                    defered.resolve(data);
                },
                function (error) {
                    defered.reject(error);
                },
                function (notify) {
                    defered.notify(notify);
                }
            );
            return {
                promise: defered.promise,
                cancel: cancel
            };
        },
        getAdDetail: function (params)
        {
            var data = {};
            /*var promise_url = params.config.APP_BASE_URL + 'getAdDetailsFull/AppKey/' + config.APP_KEY;
            if (params.adId != null)
            {
                promise_url = promise_url+'/AdID/'+params.adId;
            }
            */
            var promise_url = "../../ad-detail.json";
            var defered = $q.defer();
            var canceller = $q.defer();
            var cancel = function (reason) {
                canceller.resolve(reason);
            };

            $http.get(promise_url,
                {
                    //params: data,
                    timeout: canceller.promise,
                    headers: {
                    }
                }
            ).then(
                function (data) {
                    defered.resolve(data);
                },
                function (error) {
                    defered.reject(error);
                },
                function (notify) {
                    defered.notify(notify);
                }
            );
            return {
                promise: defered.promise,
                cancel: cancel
            };
        },
        getUserAds: function(params)
        {
            var data = {};
            /*var promise_url = params.config.APP_BASE_URL + 'getUserAdsFull/AppKey/' + config.APP_KEY;
            if (params.userId != null)
            {
                promise_url = promise_url+'/UserID/'+params.userId;
            }
            */
            var promise_url = "../../user-ads.json";
            var defered = $q.defer();
            var canceller = $q.defer();
            var cancel = function (reason) {
                canceller.resolve(reason);
            };

            $http.get(promise_url,
                {
                    //params: data,
                    timeout: canceller.promise,
                    headers: {
                    }
                }
            ).then(
                function (data) {
                    defered.resolve(data);
                },
                function (error) {
                    defered.reject(error);
                },
                function (notify) {
                    defered.notify(notify);
                }
            );
            return {
                promise: defered.promise,
                cancel: cancel
            };
        }
    }
}]);
